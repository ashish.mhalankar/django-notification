from .common import *
import os

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

SECRET_KEY=os.environ['SECRET_KEY']

ALLOWED_HOSTS = ['0.0.0.0','167.71.237.128']

