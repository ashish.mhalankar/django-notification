from time import sleep
from celery import shared_task
import csv
from birthday.models import User

from django.http import HttpResponse
from django.core.mail import send_mail,EmailMessage,BadHeaderError
from openpyxl import Workbook
from openpyxl.writer.excel import save_workbook



@shared_task
def notify_cust(message):
    print('sending 10k emails')
    print(message)
    sleep(5)
    print('Email sent!!!')


@shared_task
def worker(message):
    print('Worker sending 10k emails')
    print(message)
    sleep(5)
    print('Email sent!!!')

@shared_task
def beat(message):
    print('This is Beat')
    print(message)
    print('Beat sent!!!')


@shared_task
def import_users_from_csv(message):
    print(message)
    
    csv_file='/home/vesatogo/Documents/python/notification/birthday/assets/MOCK_DATA.csv'
    with open(csv_file, 'r') as file:
        reader = csv.DictReader(file)
        for row in reader:
            username = row['username']
            email = row['email']
            first_name = row['first_name']
            last_name = row['last_name']
            date_of_birth=row['data_of_birth'],

            # Check if a user with the same username exists
            user, created = User.objects.get_or_create(username=username, defaults={'email': email})

            if created:
                user.save()

            else:
                # User with this username already exists, you might want to update the user's information here.
                user.email = email
                user.save()
                print('{first_name} Updated data')

                sleep(1)

        print('All data Update !')

@shared_task
def export_and_send_email():
    data = User.objects.all()

    # Create a new Excel workbook
    workbook = Workbook()
    worksheet = workbook.active

    # Add headers
    headers = ['first_name', 'last_name']
    worksheet.append(headers)

    # Add data
    for item in data:
        row_data = [item.first_name, item.last_name]
        print("===>"+item.first_name)
        worksheet.append(row_data)

    # Create an HTTP response with the Excel file
    workbook.save('birthday/assets/user_data.xlsx')
    print("Data Exported and mailed it.")

   
    # try:
    #     email=EmailMessage(subject="[IMP] Export data",body="Please find an attachment",from_email='admin@admin.com',
    #                    to=['ashish@gmail.com'])
    #     email.attach_file('birthday/assets/user_data.xlsx')
    #     email.send()
    #     workbook.remove('birthday/assets/user_data.xlsx')
    #     print("Data Exported and mailed it.")

    # except BadHeaderError:
    #     pass

        
