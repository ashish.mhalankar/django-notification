from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.


class User(AbstractUser):

    created_at = models.DateTimeField(auto_now_add=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=10)
    email = models.EmailField(unique=True)
    date_of_birth = models.DateField(null=True,blank=True)

    def __str__(self) -> str:
        return f'{self.first_name} {self.last_name}'
    
class SmsLogs(models.Model):

    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE)