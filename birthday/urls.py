from django.views.generic import TemplateView
from django.urls import path
from . import views

# URLConf
urlpatterns = [
    path('hello/', views.say_hello),
    path('mail/', views.upload_csv),
    path('', TemplateView.as_view(template_name='index.html'))



]