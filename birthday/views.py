from django.shortcuts import render
from .tasks import notify_cust,import_users_from_csv,export_and_send_email,worker
from django.core.mail import send_mail,mail_admins,BadHeaderError

def say_hello(request):
    worker.delay('Hello')
    return render(request, 'hello.html', {'name': 'Mosh'})

def upload_csv(request):
    # import_users_from_csv.delay("Task")
    export_and_send_email.delay()
    return render(request, 'hello.html', {'name': 'Ashish'})

def mail_send(request):
    try:
        send_mail('[IMP]Hello Subject','This is my Second Mail','info@test.com',['ashish.mhalankar@gmail.com'])
    except BadHeaderError:
        pass
    return render(request, 'hello.html', {'name': 'Email Is Sent'})
    
              
