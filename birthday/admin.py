from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from . import models
from .tasks import import_users_from_csv

# Register your models here.


def action_upload_user_csv(modeladmin, request, queryset):

    try:
        # Replace 'my_command' with your command name
        import_users_from_csv.delay("Uploading task is started !")
        modeladmin.message_user(
            request, 'Custom command executed successfully')
    except Exception as e:
        modeladmin.message_user(request, f'Error: {e}', level=admin.ERROR)


action_upload_user_csv.short_description = "Upload User .csv"


@admin.register(models.User)
class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {"fields": ("username", "password",)}),
        (("Personal info"), {"fields": (
            "first_name", "last_name", "email", 'date_of_birth', 'phone_number')}),
        (
            ("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("username", "password1", "password2", 'email', 'first_name', 'last_name'),
            },
        ),
    )
    actions = [action_upload_user_csv]


@admin.register(models.SmsLogs)
class SmsAdmin(admin.ModelAdmin):
    list_display = ['created_at', 'user']
